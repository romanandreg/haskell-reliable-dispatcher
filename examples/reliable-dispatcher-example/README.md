
# reliable-dispatcher-example

This is an example that showcases how the `reliable-dispatcher` API works

For more information about `reliable-dispatcher` API and how to use it, see
the [`reliable-dispatcher` homepage](https://github.com//Haskell-reliable-dispatcher).
