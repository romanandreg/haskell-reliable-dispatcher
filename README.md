[![Build Status](https://travis-ci.org//Haskell-reliable-dispatcher.svg?branch=master)](https://travis-ci.org//Haskell-reliable-dispatcher)
[![Github](https://img.shields.io/github/commits-since//haskell-reliable-dispatcher/v0.0.0.1.svg)](https://img.shields.io/github/commits-since//haskell-reliable-dispatcher/v0.0.0.1.svg)
[![Hackage](https://img.shields.io/hackage/v/teardown.svg)](https://img.shields.io/hackage/v/reliable-dispatcher.svg)
[![Hackage Dependencies](https://img.shields.io/hackage-deps/v/reliable-dispatcher.svg)](https://img.shields.io/hackage/v/reliable-dispatcher.svg)
[![Stackage LTS](http://stackage.org/package/reliable-dispatcher/badge/lts)](http://stackage.org/lts/package/reliable-dispatcher)
[![Stackage Nightly](http://stackage.org/package/reliable-dispatcher/badge/nightly)](http://stackage.org/nightly/package/reliable-dispatcher)
# reliable-dispatcher

> Description of what this library does

## Table Of Contents

* [Raison d'etre](#raison-detre)

## Raison d'etre

Give me some reason to care
