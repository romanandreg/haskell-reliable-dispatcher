{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Control.ReliableDispatcher
  (
    ReliableDispatchEvent(..)
  , ReliableDispatchOptions(..)
  , DispatchFailure(..)
  , FailureId
  , AttemptCount
  , DelaySeconds
  , defaultReliableOptions
  , makeReliable
  ) where

import Control.ReliableDispatcher.Internal.Core (makeReliable)
import Control.ReliableDispatcher.Internal.Types
  ( ReliableDispatchOptions (..)
  , ReliableDispatchEvent (..)
  , DispatchFailure (..)
  , FailureId
  , AttemptCount
  , DelaySeconds
  , defaultReliableOptions
  )
