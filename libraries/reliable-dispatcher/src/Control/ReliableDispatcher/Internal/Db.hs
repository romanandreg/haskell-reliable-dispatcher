{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeFamilies      #-}
module Control.ReliableDispatcher.Internal.Db where

import Protolude

import Data.Time   (UTCTime)
import Data.Map.Strict (Map)
import Data.Vector (Vector)

import qualified Data.Acid     as Acid
import qualified Data.SafeCopy as Acid (base, deriveSafeCopy)
import qualified Data.Map.Strict  as Map
import qualified Data.Vector as Vector

import Control.ReliableDispatcher.Internal.Types

newtype DispatchFailureDb
  = DispatchFailureDb
  {
    failureDbToMap :: Map FailureId DispatchFailure
  }

storeFailure :: DispatchFailure -> Acid.Update DispatchFailureDb ()
storeFailure callFailure =
  modify (\(DispatchFailureDb db) ->
            DispatchFailureDb (Map.insert (tfId callFailure) callFailure db))

dropFailure :: FailureId -> Acid.Update DispatchFailureDb ()
dropFailure failureId =
  modify (\(DispatchFailureDb db) ->
            DispatchFailureDb $
              Map.delete failureId db)

fetchFailures
  :: Int
  -> UTCTime
  -> Acid.Query DispatchFailureDb (Vector DispatchFailure)
fetchFailures limit timestamp = do
  (DispatchFailureDb db) <- ask
  return
    $ Vector.fromList
    $ take limit
    $ filter (\callFailure -> tfRetryAfter callFailure < timestamp)
    $ Map.elems db


purgeDb :: Acid.Update DispatchFailureDb ()
purgeDb =
  modify (\(DispatchFailureDb _) ->
           DispatchFailureDb Map.empty)

getDb :: Acid.Query DispatchFailureDb DispatchFailureDb
getDb = ask

emptyDb :: DispatchFailureDb
emptyDb =
  DispatchFailureDb Map.empty

Acid.deriveSafeCopy 0 'Acid.base ''DispatchFailure
Acid.deriveSafeCopy 0 'Acid.base ''DispatchFailureDb
Acid.makeAcidic
  ''DispatchFailureDb
  [ 'storeFailure
  , 'dropFailure
  , 'fetchFailures
  , 'getDb
  , 'purgeDb
  ]
