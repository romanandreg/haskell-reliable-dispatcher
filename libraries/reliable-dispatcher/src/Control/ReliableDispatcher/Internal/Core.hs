{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Control.ReliableDispatcher.Internal.Core where

import Protolude

import Control.Exception (Handler(..))
import Control.Teardown (Teardown, newTeardown)
import Control.Concurrent (forkIO, killThread)
import Data.Time          (UTCTime, addUTCTime, getCurrentTime)
import Data.Unique        (hashUnique, newUnique)

import qualified Data.Acid   as Acid
import qualified Data.Vector as Vector

import Control.ReliableDispatcher.Internal.Db
import Control.ReliableDispatcher.Internal.Types

--------------------------------------------------------------------------------

calcRetryAt
  :: (AttemptCount -> DelaySeconds)
  -> UTCTime
  -> AttemptCount
  -> UTCTime
calcRetryAt delayFn time attempt =
  addUTCTime (fromIntegral $ delayFn attempt) time

--------------------------------------------------------------------------------

newFailure
  :: ReliableDispatchOptions
  -> ByteString
  -> IO DispatchFailure
newFailure options payload = do
  transmissionId <- hashUnique <$> newUnique
  creationTime <- getCurrentTime

  let
    attemptCount = 0

  return
    DispatchFailure
    {
      tfId =
        transmissionId

    , tfAttempts =
        attemptCount

    , tfPayload =
        payload

    , tfRetryAfter =
        calcRetryAt (dispatchDelayFn options) creationTime attemptCount

    , tfCreatedAt =
        creationTime
    }

bumpFailure
  :: ReliableDispatchOptions
  -> DispatchFailure
  -> IO DispatchFailure
bumpFailure options failure = do
  let
    attemptCount =
      succ $ tfAttempts failure

  current <- getCurrentTime
  return
    failure {
      tfAttempts =
        attemptCount

    , tfRetryAfter =
        calcRetryAt (dispatchDelayFn options) current attemptCount
    }

--------------------------------------------------------------------------------

makeReliable
  :: Text     -- ^ Human readable description
  -> FilePath -- ^ Filepath where failed executions are going to be stored
  -> ReliableDispatchOptions
  -> (ByteString -> IO ()) -- ^ transmission function that may fail
  -> IO (ByteString -> IO (), Teardown)
makeReliable desc filepath options dispatchFn =
  let
    secondToMicros =
      (* 1000000)

    -- Handles all exceptions but asynchronous ones, when catching asynchronous
    -- exceptions we store errors and then re-throw the original exception
    {-# INLINE secureDispatch #-}
    secureDispatch payload =
      catches
        (Right <$> dispatchFn payload)
        [
          Handler $ \(ex :: AsyncException) ->
            return $ Left (Left $ toException ex)

        , Handler $ \(ex :: SomeException) ->
            return $ Left (Right $ toException ex)
        ]

    retryDispatchFn dispatcherDb prevFailure = mask $ \restore -> do
      restore $ onDispatchNotification options (BeforeRetry prevFailure)
      result <- restore $ secureDispatch (tfPayload prevFailure)
      case result of
        Left (Left ex) -> do
          failure <- bumpFailure options prevFailure
          Acid.update dispatcherDb (StoreFailure failure)
          Acid.createCheckpoint dispatcherDb
          restore $ do
            onDispatchNotification options (AfterRetry prevFailure)
            onDispatchNotification options (FailedRetry failure ex)
          throwIO ex

        Left (Right ex) -> do
          failure <- bumpFailure options prevFailure
          unless (shouldStopDispatchRetry options failure ex) $ do
            Acid.update dispatcherDb (StoreFailure failure)
            Acid.createCheckpoint dispatcherDb
          restore $ do
            onDispatchNotification options (AfterRetry prevFailure)
            onDispatchNotification options (FailedRetry failure ex)
            onDispatchNotification options (AsyncError failure ex)

        Right _ -> do
          Acid.update dispatcherDb (DropFailure $ tfId prevFailure)
          Acid.createCheckpoint dispatcherDb
          restore $ do
            onDispatchNotification options (AfterRetry prevFailure)
            onDispatchNotification options (SuccessRetry prevFailure)

    reliableDispatchFn dispatcherDb payload = do
      result <- secureDispatch payload
      case result of
        Left (Left ex) -> do
          failure <- newFailure options payload
          Acid.update dispatcherDb (StoreFailure failure)
          Acid.createCheckpoint dispatcherDb
          onDispatchNotification options (FailedRetry failure ex)
          throwIO ex

        Left (Right ex) -> do
          failure <- newFailure options payload
          Acid.update dispatcherDb (StoreFailure failure)
          Acid.createCheckpoint dispatcherDb
          onDispatchNotification options (FailedDispatch failure ex)

        Right _ ->
          onDispatchNotification options (SuccessDispatch payload)

    -- TODO: Improve upon this by having a WorkerManager that
    -- fetches the failures and then distributes it in a group
    -- of worker threads (maybe using a semaphore or something)
    retryDispatchWorker dispatcherDb = forever $ do
      current <- getCurrentTime

      failuresToRetry <-
        Acid.query
         dispatcherDb
         (FetchFailures (numberOfDispatchPerBlock options) current)

      Vector.mapM_ (retryDispatchFn dispatcherDb) failuresToRetry
      threadDelay (secondToMicros $ delayPerDispatchBlock options)

  in do
    dispatcherDb <-
      Acid.openLocalStateFrom filepath emptyDb

    workerFork <-
      forkIO (retryDispatchWorker dispatcherDb)

    reliableTeardown <-
      newTeardown desc [ ("worker thread" :: Text, killThread workerFork)
                       , ("acid-state database", Acid.closeAcidState dispatcherDb) ]

    return (reliableDispatchFn dispatcherDb, reliableTeardown)
