{-# LANGUAGE NoImplicitPrelude #-}
module Control.ReliableDispatcher.Internal.Types where

import Data.Time (UTCTime)
import Protolude

type FailureId = Int
type AttemptCount = Int
type DelaySeconds = Int

data DispatchFailure
  = DispatchFailure
  {
    tfId         :: !FailureId
  , tfAttempts   :: !AttemptCount
  , tfPayload    :: !ByteString
  , tfRetryAfter :: !UTCTime
  , tfCreatedAt  :: !UTCTime
  }
  deriving (Show)

data ReliableDispatchEvent
  = BeforeRetry { dispatchFailure :: !DispatchFailure }
  | AfterRetry  { dispatchFailure :: !DispatchFailure }
  | AsyncError  { dispatchFailure :: !DispatchFailure
                , dispatchException :: !SomeException }
  | SuccessDispatch { dispatchPayload :: !ByteString }
  | FailedDispatch { dispatchFailure :: !DispatchFailure
                   , dispatchException :: !SomeException }
  | SuccessRetry { dispatchFailure :: !DispatchFailure }
  | FailedRetry { dispatchFailure :: !DispatchFailure
                , dispatchException :: !SomeException }
  deriving (Show)

data ReliableDispatchOptions
  = ReliableDispatchOptions
  {
    -- | Function that receives the call attempt count and returns
    -- a delay
    dispatchDelayFn          :: !(AttemptCount -> DelaySeconds)
    -- | Function called whenever there is a retry of a previously failed
    -- dispatch
  , onDispatchNotification   :: !(ReliableDispatchEvent -> IO ())
    -- | Returns True if we want to stop the retry execution, False otherwise
    -- This should be used in combination with 'onDispatchError' to store the
    -- error in a dead letter queue with better persistent guarantees
  , shouldStopDispatchRetry  :: !(DispatchFailure -> SomeException -> Bool)
    -- | Time the worker will wait before querying the error DB and perform a
    -- dispatch of failed entries
  , delayPerDispatchBlock    :: !DelaySeconds
    -- | Number of dispatchs to retry per attempt
  , numberOfDispatchPerBlock :: !Int

  }

defaultReliableOptions :: ReliableDispatchOptions
defaultReliableOptions =
  ReliableDispatchOptions
  {
    dispatchDelayFn = (^ (2 :: Int))
  , onDispatchNotification = \_ -> return ()
  , shouldStopDispatchRetry = \_ _ -> False
  , delayPerDispatchBlock = 30
  , numberOfDispatchPerBlock = 100
  }
