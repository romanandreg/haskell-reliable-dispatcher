{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Main where

import Protolude

import Criterion
import Criterion.Main

import qualified Data.ByteString as BS
import Control.ReliableDispatcher (makeReliable, defaultReliableOptions)
import Control.Teardown (teardown)

import Control.DeepSeq (NFData(..))

instance NFData a => NFData (IO a) where
  rnf _action = ()

main :: IO ()
main =
  let
    consumeBs _ =
      return ()
  in
    defaultMain [
      envWithCleanup
        (do (consumeBs', reliableTeardown) <-
              makeReliable "test"
              "tmp/benchmark_db"
              defaultReliableOptions
              consumeBs
            return (consumeBs', void $ teardown reliableTeardown)
        )

        (\(_, runTeardown) ->
            runTeardown
        )

        (\ ~(consumeBs', _) ->
            bgroup "main" [
              bench "simple IO unit return" (whnfIO $ consumeBs BS.empty)
            , bench "reliable IO return" (whnfIO $ consumeBs' BS.empty)
            ]
        )
    ]
