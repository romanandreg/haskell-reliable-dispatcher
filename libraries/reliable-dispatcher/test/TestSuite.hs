{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Protolude

import Test.Tasty                   (TestTree, defaultMainWithIngredients, testGroup)
import Test.Tasty.HUnit             (testCase, assertEqual, assertFailure)
import Test.Tasty.Ingredients.Rerun (rerunningTests)
import Test.Tasty.Runners           (consoleTestReporter, listingTests)

import Data.IORef (IORef, atomicModifyIORef, newIORef)
import Control.Concurrent.MVar (newEmptyMVar, putMVar, takeMVar)

import Data.Vector (Vector)
import Data.Map.Strict (Map)

import Control.Teardown (teardown)
import qualified Data.Map.Strict as Map
import qualified Data.Acid as Acid
import qualified Data.Vector as Vector

import Control.ReliableDispatcher
import Control.ReliableDispatcher.Db

main :: IO ()
main =
  defaultMainWithIngredients
    [ rerunningTests [listingTests, consoleTestReporter] ]
    ( testGroup "reliable-dispatcher" tests )

tests :: [TestTree]
tests =
  [ testGroup "makeReliable" makeReliable_tests
  ]


--------------------------------------------------------------------------------

getDb :: FilePath -> IO (Map FailureId DispatchFailure)
getDb filepath =
  bracket (Acid.openLocalStateFrom filepath emptyDb)
          (Acid.closeAcidState)
          (\db -> failureDbToMap <$> Acid.query db GetDb)

purgeDb :: FilePath -> IO ()
purgeDb filepath =
  bracket (Acid.openLocalStateFrom filepath emptyDb)
          (Acid.closeAcidState)
          (\db -> void $ Acid.update db PurgeDb)

--------------------------------------------------------------------------------

data InvalidDispatchException
  = InvalidDispatchException
  deriving (Show)

instance Exception InvalidDispatchException

data NotificationAssertion
  = NotificationAssertion
  {
    naDescription :: !ByteString
  , naAssert      :: !(ReliableDispatchEvent -> IO Bool)
  }

--------------------------------------------------------------------------------

successRetry :: ByteString -> NotificationAssertion
successRetry expectedPayload =
  NotificationAssertion
    ("SuccessRetry (" <> expectedPayload <> ")")
    (\notification ->
       case notification of
         SuccessRetry failure -> do
           assertEqual "SuccessRetry / payload should be the same"
                       expectedPayload (tfPayload failure)
           return True
         _ ->
           return False)

successDispatch :: ByteString -> NotificationAssertion
successDispatch expectedPayload =
  NotificationAssertion
    ("SuccessDispatch (" <> expectedPayload <> ")")
    (\notification ->
       case notification of
         SuccessDispatch payload -> do
           assertEqual "SuccessDispatch / payload should be the same"
                       expectedPayload payload
           return True
         _ ->
           return False)

failedDispatch :: ByteString -> NotificationAssertion
failedDispatch expectedPayload =
  NotificationAssertion
    ("FailedDispatch (" <> expectedPayload <> ")")
    (\notification ->
       case notification of
         FailedDispatch failure _err -> do
           assertEqual "FailedDispatch / payload should be the same"
                       expectedPayload (tfPayload failure)
           return True
         _ ->
           return False)

failedRetry :: ByteString -> NotificationAssertion
failedRetry expectedPayload =
  NotificationAssertion
    ("FailedRetry (" <> expectedPayload <> ")")
    (\notification ->
       case notification of
         FailedRetry failure _err -> do
           assertEqual "FailedRetry / payload should be the same"
                       expectedPayload (tfPayload failure)
           return True
         _ ->
           return False)

--------------------

_collectNotifications
  :: IORef (Vector ReliableDispatchEvent)
  -> ReliableDispatchEvent
  -> IO ()
_collectNotifications accRef notification =
  atomicModifyIORef accRef (\acc -> (Vector.snoc acc notification, ()))

_assertNotifications
  :: [NotificationAssertion]
  -> [ReliableDispatchEvent]
  -> IO ()
_assertNotifications assertions notifications =
  case (assertions, notifications) of
    ([], _) ->
      return ()

    (_, []) ->
      assertFailure $
        "Missing expected notifications: "
        <> show (map naDescription assertions)

    (assertion:as, notification:ns) -> do
      matched <- naAssert assertion notification
      if matched then
        _assertNotifications as ns
      else
        _assertNotifications assertions ns

buildNotificationAsserter
  :: IO ( ReliableDispatchEvent -> IO ()
        , [NotificationAssertion] -> IO ()
        )
buildNotificationAsserter = do
  accRef <- newIORef Vector.empty
  let
    collector =
      _collectNotifications accRef

    asserter assertions = do
      notifications <- atomicModifyIORef accRef (\current -> (Vector.empty, current))
      _assertNotifications assertions (Vector.toList notifications)

  return (collector, asserter)

buildSuccessRetryChecker
  :: IO ( ReliableDispatchEvent -> IO ()
        , IO ReliableDispatchEvent )
buildSuccessRetryChecker = do
  doneVar <- newEmptyMVar
  return (\notification ->
            case notification of
              SuccessRetry {} ->
                putMVar doneVar notification
              _ ->
                return ()
         , takeMVar doneVar
         )

--------------------------------------------------------------------------------

flakyDispatch :: Bool -> ByteString -> IO ()
flakyDispatch shouldFail _payload =
  if shouldFail then do
    throwIO InvalidDispatchException
  else
    return ()

timedDispatch :: Bool -> DelaySeconds -> ByteString -> IO ()
timedDispatch shouldFail delaySeconds _payload = do
  if shouldFail then
    throwIO InvalidDispatchException
  else do
    threadDelay delaySeconds
    return ()

--------------------------------------------------------------------------------

makeReliable_tests :: [TestTree]
makeReliable_tests =
  [
    testCase "persists and then retries failures from previous executions" $ do
      let
        dbName =
          "./tmp/flaky_dispatch"

      purgeDb dbName

      (collectNotifications, assertNotifications) <-
        buildNotificationAsserter

      -- 1st RUNTIME EXECUTION
      -- Pristine state
      (dispatch1, flakyTeardown1) <-
        makeReliable
          "flakyDispatch"
          dbName
          (defaultReliableOptions
           {
             onDispatchNotification = collectNotifications
           })
          (flakyDispatch True)

      dispatch1 "1st dispatch"

      -- Shutdown reliable resources to retry later
      -- give enough time to propagate and then clean up
      void $ teardown flakyTeardown1

      db1 <- getDb dbName
      assertEqual ("database should have 1 failing record\n" <> show (Map.elems db1))
                  1 (Map.size db1)
      assertNotifications [ failedDispatch "1st dispatch" ]

      -- 2nd RUNTIME EXECUTION
      -- Execution with errors that have been recorded before

      (collectSuccessNotification, checkSuccessNotification) <-
        buildSuccessRetryChecker

      (dispatch2, flakyTeardown2) <- makeReliable
        "flakyDispatch"
        dbName
        (defaultReliableOptions
        {
           onDispatchNotification = \notification -> do
             collectNotifications notification
             collectSuccessNotification notification
        })
        (flakyDispatch False)

      dispatch2 "2nd dispatch"

      -- Shutdown reliable resources for cleanup
      void $ checkSuccessNotification
      void $ teardown flakyTeardown2

      dbSize2 <- Map.size <$> getDb dbName
      assertEqual "database should have no failing record (all retried)"
                  0 dbSize2
      assertNotifications [ successDispatch "2nd dispatch"
                          , successRetry "1st dispatch" ]


  , testCase "teardown procedure guarantees payloads in flight are persisted" $ do
      let
        dbName =
          "./tmp/delay_dispatch"

      purgeDb dbName

      (collectNotifications, assertNotifications) <-
        buildNotificationAsserter

      -- 1st RUNTIME EXECUTION
      (dispatch1, timedTeardown1) <- makeReliable
        "delayDispatch"
        dbName
        (defaultReliableOptions
         {
           onDispatchNotification =
             collectNotifications
         })
        (timedDispatch True 5100100)

      dispatch1 "timed 1st dispatch"
      void $ teardown timedTeardown1

      db1 <- getDb dbName
      assertEqual ("database should have 1 failing record\n" <> show (Map.elems db1))
                  1 (Map.size db1)

      -- 2nd RUNTIME EXECUTION
      -- In this case, the previously failed error
      -- is being executed on the retry worker, which is going
      -- to wait for 5 seconds and then is going to be teardown
      -- by the testsuite
      (_dispatch2, timedTeardown2) <- makeReliable
        "delayDispatch"
        dbName
        (defaultReliableOptions
         {
           onDispatchNotification =
             collectNotifications

         })
        (timedDispatch False 5100100)

      -- Give some leeway for the retry worker to catch the job
      threadDelay 900

      void $ teardown timedTeardown2
      assertNotifications [ failedDispatch "timed 1st dispatch" ]

      db2 <- getDb dbName
      assertEqual ("database should still have 1 failing record\n" <> show (Map.elems db2))
                  1 (Map.size db2)

  ]
